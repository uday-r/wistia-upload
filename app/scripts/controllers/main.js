'use strict';

/**
 * @ngdoc function
 * @name videoUploadApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the videoUploadApp
 */
angular.module('videoUploadApp')
  .controller('MainCtrl', ['$scope', '$mdSidenav', '$mdDialog',function ($scope, $mdSidenav, $mdDialog) {

	  $scope.uploadedVideos = [];

	  $scope.showVideo = function(video) {
		  $mdDialog.show({
			  controller: 'VideoCtrl',
			  templateUrl: '/views/videoContainer.html',
			  locals: {
				  video: video
			  }
		  });
	  }
  }]);


angular.module('videoUploadApp')
	.controller('VideoCtrl', ['$scope', '$mdDialog', 'video', '$sce', function($scope, $mdDialog, video, $sce) {

		$scope.url = $sce.trustAsResourceUrl('http://fast.wistia.net/embed/iframe/' + video.hashed_id);
		$scope.close = function() {
			$mdDialog.cancel();
		}

	}]);
