'use strict';

/**
 * @ngdoc overview
 * @name videoUploadApp
 * @description
 * # videoUploadApp
 *
 * Main module of the application.
 */
angular
  .module('videoUploadApp', [
    'ngAnimate',
    'ngAria',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
	'ngMessages',
	'ngMaterial',
	'ngFileUpload'
  ])
  .config(function ($routeProvider, $mdThemingProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .otherwise({
        redirectTo: '/'
      });


	  $mdThemingProvider.theme('default')
	  .primaryPalette('deep-purple', {
		  'default': '500',
		  'hue-1': '700',
		  'hue-2': '300',
		  'hue-3': '50'
	  }).accentPalette('green', {
		  'default': '500',
	  'hue-1': '700',
	  'hue-2': 'A200'
	  }).warnPalette('red', {
		  'default': 'A700',
	  'hue-1': '500',
	  'hue-2': '300',
	  'hue-3': '400'
	  });

  });
