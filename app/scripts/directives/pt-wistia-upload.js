var app = angular.module('videoUploadApp')

app.directive('ptWistiaUpload', function() {
	return {
		templateUrl: '/views/pt-wistia-upload.html',
		controller: 'UploadCtrl',
		scope: {
			uploadedVideos: '=uploadedVideos'
		},
		link: function(scope, ele, attrs) {
			scope.password = attrs.apiPassword;
			scope.uploadedImage = "images/upload.jpg";
		}
	}
});

app.controller('UploadCtrl', 
	[
		'$scope'
		, 'Upload'
		, '$mdToast'
			, function($scope
			, $upload
			, $mdToast) {
			
				$scope.uploadFiles = function(files){
					$scope.files = files;
					$scope.uploadStarted = true;
					if (!$scope.files) return;
					angular.forEach(files, function(file){
						if (file && !file.$error) {
							file.upload = $upload.upload({
								url: "https://upload.wistia.com/",
								data: {
									'file': file,
									'api_password': $scope.password,
									'project_id': '8l0afzgi5w'
								}
							}).progress(function (e) {
								$scope.progress = Math.round((e.loaded * 100.0) / e.total);
								file.status = "Uploading... " + file.progress + "%";
							}).success(function (data, status, headers, config) {
								$scope.uploadStarted = false;
								$scope.uploadedVideos.push(data);
							}).error(function (data, status, headers, config) {
								$scope.uploadStarted = false;
								file.result = data;
								$mdToast.show($mdToast.simple().textContent("Looks like the video upload limit has been reached!"));
							});
						}
					});
				};


			}]);
